
all:
	@tools/rebuild -C;

clean:
	@for f in $$(tools/list-projects); do\
		(cd "$${f}" && make clean) >/dev/null;\
	done;
